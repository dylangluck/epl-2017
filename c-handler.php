<?php
  if (isset($_REQUEST["email"]))  {

    $to_email = "db@eplabs.co";

    $email = filter_var($_REQUEST["email"], FILTER_SANITIZE_EMAIL);
    $name = filter_var($_REQUEST["name"], FILTER_SANITIZE_STRING);
    $company = filter_var($_REQUEST["company"], FILTER_SANITIZE_STRING);
    $message = filter_var($_REQUEST["message"], FILTER_SANITIZE_STRING);

    $subject = "New message from " . $name . " at " . $company;
    $content = "Name: " . $name . "\n" .
               "Email: " . $email . "\n" .
               "Company: " . $company . "\n\n" .
               "Message:" . "\n" .
               $message . "\n\n";

    mail($to_email, $subject, $content);


  }
?>
