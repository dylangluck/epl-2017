//
// EPL Section Animation
//

// ScrollMagic Controller
var animateContentCtlr = new ScrollMagic.Controller();

// Elements
var $animateContentContainers = $('.epl-animate-content');

// Loop through each line
$animateContentContainers.each(function(x, container){

  // Elements
  $container = $(container);
  $containerTargets = $container.find('.epl-animate-content__target');

  $containerTargets.each(function(y, target){

    // Elements
    $target = $(target);

    // Timeline
    var targetTl = new TimelineMax({ paused:true });

    // Set Before State
    TweenLite.set($target, {y: 20, opacity: .5});

    // Define Animation
    targetTl
      .to($target, .8, {y: 0, opacity: 1, ease: 'ease-in'}, '=-.1');

    // Scroll Trigger

    var targetScene = new ScrollMagic.Scene({
      triggerElement: $target[0],
      triggerHook: .8,
      reverse: false
    })
    .on('start', function () {
        targetTl.play(0);
    })
    .addTo(animateContentCtlr);

  });

});
