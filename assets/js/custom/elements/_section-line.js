//
// Section Line
//

// ScrollMagic Controller
var sectionLineCtlr = new ScrollMagic.Controller();

// Elements
var $sectionLines = $('.section-line');

// Loop through each line
$sectionLines.each(function(i, el){

  // Element
  $el = $(el);

  // Timeline
  var sectionLineTl = new TimelineMax({ paused:true });

  // Set Before State
  TweenLite.set($el, {width: '0%'});

  // Define Animation
  sectionLineTl
    .to($el, .3, {width: '14%', ease: 'ease-in'}, '+=.1');

  // Scroll Trigger

  var sectionLineScene = new ScrollMagic.Scene({
    triggerElement: $el[0],
    triggerHook: .8,
    reverse: false
  })
  .on('start', function () {
      sectionLineTl.play(0);
  })
  .addTo(sectionLineCtlr);

});
