//
// Contact Form AJAX
//

// Elements
var $contactForm = $('.epl-form');
var $submitButton = $contactForm.find('input[type=submit]');
var $error = $('.epl-form__error');
var $success = $('.epl-contact__success');

$contactForm.on('submit', function(e){

  e.preventDefault();

  // values
  var email = $('input[name=email]').val();
  var name = $('input[name=name]').val();
  var company = $('input[name=company]').val();
  var message = $('textarea[name=message]').val();

  if(email && message) {

    $.post({
      url: '/c-handler.php',
      data: {
        email: email,
        name: name,
        company: company,
        message: message
      }
    }).done(function(){

      $error.hide();
      $contactForm.hide();
      $success.show();

    });

  } else {

    $error.show();
    $submitButton.removeClass('active');

  }

  return false;

});


// Submit active
var $inputs = $contactForm.find('input, textarea');

$inputs.on('keyup', function(){
  var count = 0;

  $.each($inputs, function(i, $input){
    if($input.val().length > 1) {
      count++;
    }
  });

  if(count == $inputs.length) {
    $submitButton.addClass('active');
  } else {
    $submitButton.removeClass('active');
  }
});
