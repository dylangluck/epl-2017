//
// Modal Logic
//

// Elements

var $modalLinks = $('.modal-link');
var $modals = $('.epl-modal');

$modalLinks.on('click', function(){

  var $link = $(this);
  var linkTarget = $link.attr('data-target');

  // Filter Modal
  var $modal = $modals.filter(function() {
    return $(this).attr('data-modal-id') == linkTarget;
  });

  // Show animation
  $modal.show();

  // Attach Close Button
  var $modalClose = $modal.find('.epl-modal__close');

  $modalClose.on('click', function(){
    $modal.hide();
  });

  // Attach Esc key
  $(document).keyup(function(e) {
    if(e.keyCode === 27){
      $modal.hide();
      $(document).off('keyup');
    }
  });

});
