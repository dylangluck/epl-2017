//
// Global Variables
//

// Elements
var $window = $(window);
var $document = $(document);
var $body = $('body');

//
// Page Loader
//

// Elemets
var $loader = $('.epl-loader');

$(window).on('load', function(){

  // Fade out loader
  $loader.animate({opacity: 0}, 600);
  setTimeout(function(){

    // Hide Element
    $loader.hide();

    // Trigger Event
    $window.trigger('load-done');

  }, 600);

});

//
// Device Info
//

// Is Touch Device ?
var isMobile = Modernizr.touch;

// Blend Mode?
Modernizr.addTest('mix-blend-mode', function(){
    return Modernizr.testProp('mixBlendMode');
});


//
// Navigation
//

// Elements
var $scrollLinks = $('.epl-scroll-link');

$scrollLinks.on('click', function(e){

  e.preventDefault();

  var $el = $(this);
  var target = $el.attr('href');

  var scrollLocation = $(target).offset().top;

  $('html,body').animate({scrollTop: scrollLocation}, 1200);

  return false;

});


//
// Mobile Navigation
//

// Init panel
var mobileNavigation = $('#mobile-navigation').scotchPanel({
  containerSelector: '.epl-hero-mobile-wrap', // Make this appear on the entire screen
  direction: 'right', // Make it toggle in from the left
  duration: 300, // Speed in ms how fast you want it to be
  transition: 'ease', // CSS3 transition type: linear, ease, ease-in, ease-out, ease-in-out, cubic-bezier(P1x,P1y,P2x,P2y)
  distanceX: '50%', // Size fo the toggle
  enableEscapeKey: true, // Clicking Esc will close the panel
  beforePanelOpen: function() {
    // $('.mms-navigation').addClass('nav-open');
  },
  beforePanelClose: function() {
    //$('.mms-navigation').removeClass('nav-open');
  }
});

// Mobile Navigation Trigger
$('#mobile-navigation-toggle').click(function(){
  // Toggle Drawer
  mobileNavigation.toggle();
  // Toggle Hamburger
  $(this).find('i').toggleClass('fa-bars fa-times');
});

// Clear Flash of Content
$window.on('load', function(){
  $('#mobile-navigation').css('visibility', 'visible')
});

// Close Mobile Nav on Link Click
$('#mobile-navigation a').on('click', function(){
  $('#mobile-navigation-toggle').trigger('click');
});
