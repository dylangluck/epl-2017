//
// Homepage Hero
//

// ScrollMagic Controller
var heroCtlr = new ScrollMagic.Controller();

// Elements
var $hero = $('.epl-hero');
var $heroNavigation = $hero.find('.epl-navigation');
var $heroH1 = $hero.find('h1');
var $heroH1Line = $hero.find('.epl-hero__content .line');


// Timeline
var heroTl = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($heroNavigation, {y: 10, opacity: 0});
TweenLite.set($heroH1, {y: 10, opacity: 0});
TweenLite.set($heroH1Line, {width: 0});

// Define Animation
heroTl
  .to($heroNavigation, 1, {y: 0, opacity: 1, ease: 'ease-in'}, '+=.2')
  .to($heroH1, 1, {y: 0, opacity: 1, ease: 'ease-in'}, '-=.6')
  .to($heroH1Line, .4, {width: 210, ease: 'ease-in'}, '-=.5');

$window.on('load-done', function(){
  heroTl.play(0);
});
